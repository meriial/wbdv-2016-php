<pre>
<?php

$var = 'Hello';

$var .= ", World!";

$num = 3;
$num += 4;
$num = $num + 4;

$boolean = true;

$array = ["bob", "jane"];

class Car {}

$obj = new Car;
echo "hi\n\t";
var_dump(
  $boolean,
  $var,
  $num,
  $array,
  $obj
);
