<pre><?php

class Foo {

  public $bar = 'hi';

  public function greet()
  {
    echo $this->bar;
  }

}

$obj = new Foo;
echo $obj->bar."\n";
$obj->greet();
