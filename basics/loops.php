<pre>
<?php

for ($i=0; $i <= 10; $i++) {
  echo $i."\n";
}

$array = [1,4,"bob",6];

for ($i=0; $i < count($array); $i++) {
  echo $array[$i]."\n";
}

foreach ($array as $value) {
  echo $value."\n";
}

$a = 0;
do {
  echo $array[$a]."\n";
  $a++;
} while ($a < count($array));
