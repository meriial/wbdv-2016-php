<pre><?php

$a = 2;
$b = true;

var_dump($a, $b);

if ($a == 1) {
  echo "1!\n";
} elseif($a == 2) {
  echo "2!";
} else {
  echo "default";
}

switch ($a) {
  case 1:
    echo "1!";
    break;

  case 2:
    echo "2!";
    break;

  default:
    echo "default";
    break;
}
