<pre><?php

$a = ["jane", "jack","bob"];

var_dump($a, $a[2]);

$b = [
  'jane' => 'blue',
  'jack' => 'black',
  'bob'  => 'green'
];

var_dump($b);

$b['phil'] = 'yellow';
unset($b['jane']);
$b['jack'] = 'red';

var_dump($b);

foreach ($b as $color) {
  echo "favourite color is $color\n";
}

var_dump(['bob'=>1,2] == ['bob'=>1,2]);
