<?php

  require_once 'functions/forms.php';
  require_once 'functions/output.php';

  $errors = [];

  if(formWasSubmitted()) {
    if(passwordWasNotValid()) {
      $errors[] = 'ERROR! Incorrect password.';
    } else if(emailWasNotValid()) {
      $errors[] = 'ERROR! Incorrect email.';
    } else {
      redirectTo('main.php');
    }
  }

  layout('templates/login.php', $errors);
