<?php

  require_once 'functions/forms.php';

  if(formWasSubmitted()) {
    header('Location: login.php');
  }

  require 'templates/registration.php';
