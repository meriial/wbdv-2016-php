<?php

function layout($templateName, $errors)
{
  ob_start();
  require $templateName;
  $content = ob_get_clean();

  require 'templates/layout.php';
}
