<?php

function formWasSubmitted()
{
  return !empty($_POST);
}

function redirectTo($destination)
{
  header('Location: '.$destination);
}

function submittedValue($field)
{
  if(!empty($_POST[$field])) {
     return $_POST[$field];
  }

  return '';
}

function passwordWasNotValid()
{
  return empty($_POST['password'])
      || $_POST['password'] != 'correct-password';
}

function emailWasNotValid()
{
  return empty($_POST['email'])
      || $_POST['email'] != 'correct@email.com';
}
