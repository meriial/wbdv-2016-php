<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
</head>
<body>
  <header>
    <h1>Tweeter.com</h1>
    <ul class="nav">
      <li><a href="tweet.php">Go to tweets</a></li>
      <li><a href="about.php">Go to about</a></li>
    </ul>
  </header>
  <?php echo $content ?>
  <footer>
    This is the footer.
  </footer>
  <h3>Google Analytics</h3>
</body>
</html>
