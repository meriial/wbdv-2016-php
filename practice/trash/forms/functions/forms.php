<?php

function formWasSubmitted()
{
  return !empty($_POST);
}

function submittedValue($field)
{
  if(formWasSubmitted()) {
    return $_POST[$field];
  }
  return '';
}

function validateTweetForm() {
  $errors = '';

  if(formWasSubmitted()) {
    if( strlen($_POST['tweet']) > 10 ) {
      $errors .= 'ERROR! Tweet is longer than 10 characters.';
    } else {
      header('Location: tweet.php');
    }
  }

  return $errors;
}
